# I have imported statistics from the python standard library for the purpose of calculating mean of co2_values
from  statistics import mean
import pandas

# the operations function take path of the co2_alaska and co2_hawaii files and returns a dictionary.
# This dictionary has year as the key and mean, max and % change as its values in a list of lists

def operations(path):
    valid_data = [] # to filter out the required data from both files
    years = [] # to store distinct years in both files
    dict = {}
    calculations = {}

    file = open(path)

    # in this loop, I am checking the first line which is a record and valid i.e. doesn't start with # and qc_flag doesn't start with '*'
    for line in file:
        if line[0] != '#' and line.split()[-1][0]!= '*':
            # print(line.split())
            valid_data.append(line.split())
    # print((valid_data))

    # parsing over the entire valid_data, I am creating a dictionary and appending co2_values for each year
    # the if condition checks each year as a key in the dictionary. If this key is not present, then it creates the year as key and assigns co2_value to this key.
    # if the year exists in the dictionary, then it simply appends the associated co2_value to the dictionary
    for i in valid_data:
        if int(i[1]) not in dict:
            dict[int(i[1])] = [float(i[7])]
        else:
            dict[int(i[1])].append(float(i[7]))
    # print(dict)

    # this part of the code appends maximum and mean co2 values to the calculations dictionary
    for year, co2 in dict.items():
        calculations[year] = [max(co2), round(mean(co2),2)]
    # print(calculations)

    years = list(calculations.keys())

    # if it is the first year, then there is no percentage change, hence I am appending '0' to the calculations dictionary
    # otherwise, I am appending the % change as per given formula
    for i in years:
        if i == min(years):
            calculations[i].append(0)
        else:
            calculations[i].append((round((((calculations[i][1] - calculations[i-1][1])/calculations[i-1][1])*100),2)))
    # print(calculations)
    return calculations

# I tried to dynamically take input from the user
# hawaii = operations(input("Enter path for hawaii file "))
# alaska = operations(input("Enter path for alaska file "))

hawaii = operations(r'C:\Users\yash0\OneDrive\Desktop\PR\Assignment_1\co2_hawaii.txt')
alaska = operations(r'C:\Users\yash0\OneDrive\Desktop\PR\Assignment_1\co2_alaska.txt')

co2_report = open('co2_report.txt', 'w')
co2_report.write("{:<32} {}\n".format('Alaska', 'Hawaii'))
co2_report.write("{:<7} {:<10} {:<12} {:<8} {:<10} {:<10} {:<10}\n".format('YEAR', 'MAX_LEVEL', 'MEAN_LEVEL', '%CHANGE', 'MAX_LEVEL', 'MEAN_LEVEL', '%CHANGE'))

for year in (set(hawaii.keys()) | set(alaska.keys())):
    if year in alaska.keys() and year not in hawaii.keys():
        co2_report.write("""{:<7} {:<10} {:<12} {:<8} {:<10} {:<10} {:<10}\n""".format(str(year), str(alaska[year][0]), str(alaska[year][1]), ' NA', ' NA', ' NA', ' NA'))
    elif year in hawaii.keys() and year not in alaska.keys():
        co2_report.write("""{:<7} {:<10} {:<12} {:<8} {:<10} {:<10} {:<10}\n""".format(str(year), str(alaska[year][0]),str(alaska[year][1]), str(alaska[year][2]), str(hawaii[year][0]), str(hawaii[year][1]), ' NA'))
    elif year in alaska.keys() and hawaii.keys():
        co2_report.write("{:<7} {:<10} {:<12} {:<8} {:<10} {:<10} {:<10}\n".format(str(year), str(alaska[year][0]), str(alaska[year][1]), str(alaska[year][2]), str(hawaii[year][0]), str(hawaii[year][1]), str(hawaii[year][2])))

change_hawaii = [i[2] for i in hawaii.values()]
change_alaska = [i[2] for i in alaska.values()]

co2_report.write("\n")
co2_report.write("Mean of the annual % change for Hawaii is : " + str(round(mean(change_hawaii), 2)) + "\n")
co2_report.write("Mean of the annual % change for Alaska is : " +str(round(mean(change_alaska),2)))

